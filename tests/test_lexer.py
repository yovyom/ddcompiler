import ddcompiler as ddc


def test_next_peek():
    input = "1 == 1"
    ddlexer = ddc.DDLexer(input)

    output = ddlexer.current_char
    while ddlexer.peek() != ddlexer.END_OF_EXPRESSION:
        ddlexer.next_char()
        output += ddlexer.current_char

    print(f"input: {input}, output: {output}")
    assert input == output


# tests expression for appropriate tokens "abc" == "def"
def test_expression1():
    input = "\"abc\" ==        \"def\""
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)

    assert tokens[0].ttype == ddc.TokenType.STRING and tokens[0].ttext == "abc"\
        and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "def"


# tests expression for appropriate tokens "a'bc" == "de?f"
def test_expression2():
    input = "\"a'bc\" != \"de?f\""
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)

    assert tokens[0].ttype == ddc.TokenType.STRING and tokens[0].ttext == "a'bc"\
        and tokens[1].ttype == ddc.TokenType.NOTEQ and tokens[1].ttext == "!="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "de?f"


# tests expression for appropriate tokens '${conv.lang} == "fr" and True or 1 == 2 or False'
def test_expression3():
    input = '${conv.lang} == "fr" and True or 1 == 2 or False'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)

    assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.lang"\
        and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "fr"\
        and tokens[3].ttype == ddc.TokenType.AND and tokens[3].ttext == "and"\
        and tokens[4].ttype == ddc.TokenType.TRUE and tokens[4].ttext == "True"\
        and tokens[5].ttype == ddc.TokenType.OR and tokens[5].ttext == "or"\
        and tokens[6].ttype == ddc.TokenType.NUMBER and tokens[6].ttext == "1"\
        and tokens[7].ttype == ddc.TokenType.EQ and tokens[7].ttext == "=="\
        and tokens[8].ttype == ddc.TokenType.NUMBER and tokens[8].ttext == "2"\
        and tokens[9].ttype == ddc.TokenType.OR and tokens[9].ttext == "or"\
        and tokens[10].ttype == ddc.TokenType.FALSE and tokens[10].ttext == "False"


# tests wrong = operator
def test_eq():
    input = "True = False"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    try:
        token = ddlexer.get_token()
        tokens.append(token)
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert(str(ve).startswith("Error during lexical analysis: Expected ==, got ="))


# tests wrong ! operator
def test_neq():
    input = "True !> True"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    try:
        token = ddlexer.get_token()
        tokens.append(token)
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert(str(ve).startswith("Error during lexical analysis: Expected !=, got !>"))


# tests incomplete string, should raise a ValueError
def test_string_token3():
    input = "\"ab"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    try:
        token = ddlexer.get_token()
        tokens.append(token)
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert(str(ve).startswith("Error during lexical analysis: Incomplete string"))


# Strings with single quotes should result in an error
def test_string_token4():
    input = "${conv.lang} == \'fr\'"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    try:
        token = ddlexer.get_token()
        tokens.append(token)
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.lang"\
            and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="
        assert(str(ve).startswith("Error during lexical analysis: Invalid expression starting at '"))


# tests number tokens
def test_number_token1():
    input = "10.10 < 20"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)

    assert tokens[0].ttype == ddc.TokenType.NUMBER and tokens[0].ttext == "10.10"\
        and tokens[1].ttype == ddc.TokenType.LT and tokens[1].ttext == "<"\
        and tokens[2].ttype == ddc.TokenType.NUMBER and tokens[2].ttext == "20"


# tests invalid number token
def test_number_token2():
    input = "20 <= 20."
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    try:
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert tokens[0].ttype == ddc.TokenType.NUMBER and tokens[0].ttext == "20"\
            and tokens[1].ttype == ddc.TokenType.LTEQ and tokens[1].ttext == "<="
        assert(str(ve) == "Error during lexical analysis: Illegal character in number")


# tests variable
def test_variable_token1():
    input = "${conv.x1} > ${conv.a.b_c}"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.x1"\
        and tokens[1].ttype == ddc.TokenType.GT and tokens[1].ttext == ">"\
        and tokens[2].ttype == ddc.TokenType.VARIABLE and tokens[2].ttext == "conv.a.b_c"


# tests invalid variable
def test_variable_token2():
    input = "${user.x_y} >= $conv.a"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    try:
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "user.x_y"\
            and tokens[1].ttype == ddc.TokenType.GTEQ and tokens[1].ttext == ">="
        assert(str(ve) == "Error during lexical analysis: Expected { after $")


# tests invalid variable
def test_variable_token3():
    input = "${conv.a == 100"
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert(str(ve) == "Error during lexical analysis: Expected } after variable name")


# tests a valid variable name
def test_variable_token4():
    input = "${dialog.x.y} == 10.10"
    ddlexer = ddc.DDLexer(input)
    token = ddlexer.get_token()
    assert token.ttext == "dialog.x.y" and token.ttype == ddc.TokenType.VARIABLE


# tests a valid variable name
def test_variable_token5():
    input = "${user.x} == 10"
    ddlexer = ddc.DDLexer(input)
    token = ddlexer.get_token()
    assert token.ttext == "user.x" and token.ttype == ddc.TokenType.VARIABLE


# tests a valid variable name
def test_variable_token5_1():
    input = "${wf.x.y.z} == 10"
    ddlexer = ddc.DDLexer(input)
    token = ddlexer.get_token()
    assert token.ttext == "wf.x.y.z" and token.ttype == ddc.TokenType.VARIABLE


# tests a valid variable name
def test_variable_token6():
    input = "${conv.x} == 20"
    ddlexer = ddc.DDLexer(input)
    token = ddlexer.get_token()
    assert token.ttext == "conv.x" and token.ttype == ddc.TokenType.VARIABLE


# tests a valid variable name
def test_variable_token7():
    input = '${conv.x.y.z} == "success"'
    ddlexer = ddc.DDLexer(input)
    token = ddlexer.get_token()
    assert token.ttext == "conv.x.y.z" and token.ttype == ddc.TokenType.VARIABLE


# tests an invalid variable name
def test_variable_token8():
    input = '${yogi} == "success"'
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert str(ve).startswith("Error during lexical analysis: Invalid variable name yogi")


# tests an invalid variable name
def test_variable_token9():
    input = '${yogi.mogi} == "success"'
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert str(ve).startswith("Error during lexical analysis: Invalid variable name yogi.mogi")


# tests an invalid variable name
def test_variable_token10():
    input = '${dialog.x} == "success"'
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert str(ve).startswith("Error during lexical analysis: Invalid variable name dialog.x")


# tests an invalid variable name
def test_variable_token11():
    input = '${conv.__aistudio__} == "success"'
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert str(ve).startswith("Error during lexical analysis: Invalid variable name conv.__aistudio__")


# tests an invalid variable name
def test_variable_token11_1():
    input = '${wf1.xx} == "success"'
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert str(ve).startswith("Error during lexical analysis: Invalid variable name wf1.xx")


# tests some internal variable names
def test_variable_token12():
    input = '${conv.__utterance__}     == "create ticket"'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.__utterance__"\
        and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "create ticket"


def test_variable_token13():
    input = '${conv.__username__}     != "Sachin Tendulkar"'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.__username__"\
        and tokens[1].ttype == ddc.TokenType.NOTEQ and tokens[1].ttext == "!="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "Sachin Tendulkar"


def test_variable_token14():
    input = '${conv.__intent__}     == "BOOK_CONF_ROOM"'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.__intent__"\
        and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "BOOK_CONF_ROOM"


def test_variable_token15():
    inputs = [
        '${conv.__intent1__}     == "BOOK_CONF_ROOM"',
        '${conv.__user__}     == "KEVIN"',
        '${conv.__foo__}     == "FOO"'
    ]
    errors = [
        'Error during lexical analysis: Invalid variable name conv.__intent1__',
        'Error during lexical analysis: Invalid variable name conv.__user__',
        'Error during lexical analysis: Invalid variable name conv.__foo__'
    ]

    for i in range(0, len(inputs)):
        try:
            ddlexer = ddc.DDLexer(inputs[i])
            ddlexer.get_token()
        except ValueError as ve:
            assert str(ve).startswith(errors[i]), f"Index: {i}"


# test functions
def test_function_token1():
    input = 'contains("10", "1")'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.CONTAINS and tokens[0].ttext == "contains"

    input = 'contain("10", "1")'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    try:
        token = ddlexer.get_token()
    except ValueError as ve:
        assert(str(ve) == "Error during lexical analysis: 'contain' not a supported token")


def test_function_token2():
    input = 'upper("yogI") == "YOGI"'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.UPPER and tokens[0].ttext == "upper"

    input = 'ufer("yogI") == "YOGI"'
    ddlexer = ddc.DDLexer(input)
    try:
        ddlexer.get_token()
    except ValueError as ve:
        assert(str(ve) == "Error during lexical analysis: 'ufer' not a supported token")

    input = 'True and lower("yogI") == "yogi"'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[2].ttype == ddc.TokenType.LOWER and tokens[2].ttext == "lower"

    input = 'True and loafer("yogI") == "yogi"'
    ddlexer = ddc.DDLexer(input)
    try:
        token = ddlexer.get_token()
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
    except ValueError as ve:
        assert(str(ve) == "Error during lexical analysis: 'loafer' not a supported token")


def test_function_token3():
    input = 'startswith("automationedge", "automation") and endswith("automationedge", "edge")'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.STARTSWITH and tokens[0].ttext == "startswith"\
        and tokens[7].ttype == ddc.TokenType.ENDSWITH and tokens[7].ttext == "endswith"


def test_function_token4():
    input = 'len("automationedge") == 14'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.LEN and tokens[0].ttext == "len"\
        and tokens[5].ttype == ddc.TokenType.NUMBER and tokens[5].ttext == "14"


# tests boolean operators and boolean constants
def test_boolean_tokens1():
    input = '${conv.a.b} == "yogi" or ${conv.a.b} == "mogi"'
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        token = ddlexer.get_token()
        tokens.append(token)
    assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "conv.a.b"\
        and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="\
        and tokens[2].ttype == ddc.TokenType.STRING and tokens[2].ttext == "yogi"\
        and tokens[3].ttype == ddc.TokenType.OR and tokens[3].ttext == "or"\
        and tokens[4].ttype == ddc.TokenType.VARIABLE and tokens[4].ttext == "conv.a.b"\
        and tokens[5].ttype == ddc.TokenType.EQ and tokens[5].ttext == "=="\
        and tokens[6].ttype == ddc.TokenType.STRING and tokens[6].ttext == "mogi"


# tests invalid operator, OR is not allowed, it has to be or
def test_boolean_tokens2():
    input = "${user.lang} == False OR not ${user.x}"
    ddlexer = ddc.DDLexer(input)
    tokens = []
    token = ddlexer.get_token()
    tokens.append(token)
    try:
        while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
            token = ddlexer.get_token()
            tokens.append(token)
    except ValueError as ve:
        assert tokens[0].ttype == ddc.TokenType.VARIABLE and tokens[0].ttext == "user.lang"\
            and tokens[1].ttype == ddc.TokenType.EQ and tokens[1].ttext == "=="\
            and tokens[2].ttype == ddc.TokenType.FALSE and tokens[2].ttext == "False"
        assert(str(ve) == "Error during lexical analysis: 'OR' not a supported token")
