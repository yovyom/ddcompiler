import pytest
import ddcompiler as ddc


@pytest.fixture
def var_dict():
    # return a dictionary of variables
    return {
        'conv.lang': 'fr',
        'dialog.x.f1': 200.30,
        'dialog.x.i1': 1729,
        'dialog.x.y.z': "Button1",
        'user.input1': 11,
        'user.input2': 22,
        'user.input1_into_input2': 242,
        'user.input3': 300,
        'conv.input1': "100",
        'user.company': 'AutomationEdge',
        'user.product': 'AIStudio',
        'wf.x.y.z': 'create_ticket',
        'wf.success': False,
        # System variables
        'conv.__utterance__': 'some utterance',
        'conv.__username__': 'diego',
        'conv.__intent__': 'book_conf_room',
        'conv.__channel__': 'SLack',
        'conv.__skill__': 'HR',
        'conv.__debug_traces__': True,
    }


def test_parser1(var_dict):
    input = 'not not not True and ${conv.lang} == "fr" or ${conv.lang} == "de" and contains("aistudio", "studio")'
    ddparser = ddc.DDParser(ddc.DDLexer(input))
    tree = ddparser.parse()
    assert tree.evaluate(var_dict) is False


def test_parser2(var_dict):
    inputs = [
        '${conv.lang} == "fr"',
        # As ${conv.lang} is inside a string, it's not considered a variable
        '"${conv.lang}" != "fr"'
    ]

    results = [
        True,
        True
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_parse_boolean_constants(var_dict):
    inputs = [
        'True',
        'False',
        '(((True)))',
        '(False)',
        'not not not False',
        'not not True',
        'not True and False',
        'False and False or True',
        'False and (False or True)'
    ]

    results = [
        True,
        False,
        True,
        False,
        True,
        True,
        False,
        True,
        False
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_bool_function(var_dict):
    inputs = [
        'bool(${wf.success}) == False',
        'bool(${wf.success}) != not not True',
        'bool(${conv.__debug_traces__})'
    ]

    results = [
        True,
        True,
        True,
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"

    try:
        input = 'bool(${conv.lang})'
        ddparser = ddc.DDParser(ddc.DDLexer(input))
        tree = ddparser.parse()
        tree.evaluate(var_dict)
    except ValueError as ve:
        assert str(ve) == "Only boolean values can be passed to 'bool' function."


def test_parse_contains(var_dict):
    inputs = [
        # Expression
        # - not(Expression) and True
        # - not(Expression) or True
        'not not False and contains("ab", "b") or 21 > 20',
        'not(not not False and contains("ab", "b") or 21 > 20) and True',
        'not(not not False and contains("ab", "b") or 21 > 20) or True',
        # - Expression
        # - not(Expression)
        # - not(not(Expression))
        'not not False and (contains("ab", "b") or 21 > 20)',
        'not(not not False and (contains("ab", "b") or 21 > 20))',
        'not(not(not not False and (contains("ab", "b") or 21 > 20)))'
    ]
    results = [
        True,
        False,
        True,
        False,
        True,
        False
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_parse_upper_lower(var_dict):
    E = 'upper(lower(upper(${user.company}))) == "AUTOMATIONEDGE"'\
        'and lower(${user.product}) == "aistudio"'\
        'and contains("aistudio", "studio")'
    inputs = [
        # Expression
        # - False and Expression
        # - False and (Expression)
        # - not False and Expression
        E,
        f'False and {E}',
        f'False and ({E})',
        f'not False and {E}',
    ]
    results = [
        True,
        False,
        False,
        True
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_len(var_dict):
    inputs = [
        'len     (upper(lower(upper(${user.company})))) == 14',
        'len("aistudio") == 6 and True',
        'not(not(not(True))) or len("") == 0',
    ]
    results = [
        True,
        False,
        True
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_parse_conditionals(var_dict):
    inputs = [
        '${user.input1} == 11 and ${user.input2} == 22',
        '${user.input1} == 11 and ${user.input2} == 22.0',
        '${user.input1} == 11 and ${user.input2} == 22.1',
        '${user.input1_into_input2} == 242',
        '${user.input1_into_input2} != 241',
        '${user.input1_into_input2} <= 242',
        '${user.input1_into_input2} >= 242',
    ]
    results = [
        True,
        True,
        False,
        True,
        True,
        True,
        True
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_parse_misc(var_dict):
    inputs = [
        '${dialog.x.f1} > 200',
        '${dialog.x.y.z} == "Button1"',
        'contains(lower(${dialog.x.y.z}), "button1")',
        'contains(lower(upper(lower(upper("DIEGO")))), "d")',
        'upper(${wf.x.y.z}) == "CREATE_TICKET"'
    ]
    results = [
        True,
        True,
        True,
        True,
        True
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


# some tricky not use-cases
def test_tricky_not(var_dict):
    inputs = [
        'not not not True',
        'False or not not(True)',
        'True and not not not not contains("a", "ab") and True or False',
        'not(True and not not not not False and True or False)',
        'True and (not False and (not True))',
        'True and (not False or (not True))',
        'True and not not True and (True and not not False)',
        'not 1 == 1 and (not not 1 == 2 and (not not lower("Xxx") == lower("xXX")))',
        'not(not 1 == 1 and (not not 1 == 2 and (not not lower("Xxx") == lower("xXX"))))',
        'False or not not(True or not not(False))',
        'not(not(not(not(not(not(not(contains("ab", "a")))))))) != not(not(not(not(not(not(not(not(contains("ab", "a")))))))))'
    ]
    results = [
        False,
        True,
        False,
        True,
        False,
        True,
        False,
        False,
        True,
        True,
        True
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_invalid_python(var_dict):
    # Python's upper function which we use internally isn't available on int
    try:
        input = 'upper(${dialog.x.i1}) == "1729"'
        ddparser = ddc.DDParser(ddc.DDLexer(input))
        tree = ddparser.parse()
        tree.evaluate(var_dict)
    except AttributeError as ae:
        assert str(ae) == "'int' object has no attribute 'upper'"


def test_system_variables(var_dict):
    inputs = [
        '${conv.__utterance__} == "some utterance"',
        '${conv.__utterance__} != "Some utterance" and ${conv.__username__} == "diego"',
        'upper(${conv.__intent__}) == "BOOK_CONF_ROOM"',
        '${conv.__channel__} > "SLack"',
        '${conv.__skill__} == "HR" and upper(${conv.__channel__}) == "SLACK"',
        'not bool(${conv.__debug_traces__}) or False',
    ]
    results = [
        True,
        True,
        True,
        False,
        True,
        False
    ]
    for i in range(0, len(inputs)):
        ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
        tree = ddparser.parse()
        assert tree.evaluate(var_dict) is results[i], f"Index: {i}"


def test_invalid_expressions(var_dict):
    inputs = [
        '((True)',
        '((10 == 10)))',
        '10 and 20',
        'how are you',
        'upper(True)',
        'upper(contains("aistudio", "studio"))',
        '"XX" == True'
    ]
    errors = [
        "Error during parsing: Expected RPAREN, got END_OF_EXPRESSION",
        "Error during parsing: Expected END_OF_EXPRESSION, got )",
        "Error during parsing: Expected comparison operator, got and",
        "Error during lexical analysis: 'how' not a supported token",
        "Error during parsing: Unexpected True",
        "Error during parsing: Unexpected contains",
        "Error during parsing: Expected number, string, variable or identifier function, got True"
    ]
    for i in range(0, len(inputs)):
        try:
            ddparser = ddc.DDParser(ddc.DDLexer(inputs[i]))
            ddparser.parse()
        except ValueError as ve:
            assert str(ve) == errors[i], f"Index: {i}"
