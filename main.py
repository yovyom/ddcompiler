import ddcompiler as ddc
# File demonstrating usage of DDLexer and DDParser classes.
# yogesh.ketkar@automationedge.com


# DDLexer class is passed input expression. It returns a list of tokens until you get END_OF_EXPRESSION.
def test_lexer():
    input = '${conv.lang} == "fr" or ${conv.lang} == "de" and contains("aistudio", "studio")'
    ddlexer = ddc.DDLexer(input)
    token = ddlexer.get_token()
    while token.ttype != ddc.TokenType.END_OF_EXPRESSION:
        print(f"  TokenType: {token.ttype}, TokenText: {token.ttext}")
        token = ddlexer.get_token()
    print()


def test_parser():
    var_dict = {
        'conv.lang': 'fr',
        'dialog.x.y': 200.30,
        'user.input1_into_input2': 242,
        'dialog.x.y.z': "Button1",
        'wf.success': True
    }
    # Variables used in the expression of the form ${var_name} should be present in the var_dict.
    input = '${conv.lang} == "fr" or ${conv.lang} == "de" and contains("aistudio", "studio")'
    input = '${wf.success} == True or True == True and upper("yogesh") == "YOGESH"'
    input = '${wf.success} == True'
    input = 'True == ${wf.success}'
    input = 'True == not not bool(${wf.success})'
    input = 'bool(${conv.lang})'
    # input = '"xx" == "xx" or True'
    # input = 'True == not False'
    # input = '${wf.success}'
    # input = 'not not not True and ${conv.lang} == "fr" or ${conv.lang} == "de" and contains("aistudio", "studio")'
    # input = 'not not not True and ${wf.success} == True or ${wf.success} == False'
    # input = '${conv.lang} == "fr" or False'
    # input = 'True == not False'
    # input = 'True == not False'
    # input = '${wf.success}'
    # input = 'upper(True)'
    # input = 'upper(True) == "yogesh"'
    # input = 'not(not not False and (contains("ab", "b") or 21 > 20))'
    # input = 'not(not(not not False and (contains("ab", "b") or 21 > 20)))'
    # input = '${user.input1_into_input2} >= 242'
    # input = 'contains(lower(${dialog.x.y.z}), "button1")'
    # input = 'os.remove("abc")'
    # input = "${dialog.x.y} == createincident'"
    # input = 'len("") == 0'

    # input = 'not not True'
    # input = 'True and not not False and (not not True)'
    # input = 'False and False or True'
    # input = 'True and (not not True)'
    # input = 'not(not(True))'
    # input = 'not(True)'
    # input = 'True or not(True)'
    # input = '"1,234**()" == "1,234**()"'
    # input = 'endswith("abc", "abcd")'
    # input = 'contains("ab", "a")'
    # input = 'True and not not True or (not not False)'
    # input = 'not 1 == 1 and (not not 1 == 2 and (not not 1 == 3))'
    # input = 'not not not True and False and False'
    # input = 'True and not not not True and False and False'
    # input = 'True and not not not (True and False and (not not False))'

    lexer = ddc.DDLexer(input)
    ddparser = ddc.DDParser(lexer)
    tree = ddparser.parse()
    print("Parse Tree")
    tree.traverse()
    # gives list of variables in the expression in form of a set
    vars = ddparser.get_variables()
    print(f"VARIABLES in the expression: {vars}")
    print(f"INPUT: {input}")
    e = tree.evaluate(var_dict)
    print(f"EVALUATION: {e}")


if __name__ == "__main__":
    test_lexer()
    test_parser()
